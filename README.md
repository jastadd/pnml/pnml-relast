# PNML for Relational RAGs

A tool to read PNML XML files and transform them into ASTs of a relational RAG.

The tool supports place-transition nets as defined in [ISO/IEC 15909-1:2019](https://www.iso.org/standard/67235.html).

Reading is done using the [PNML Framework](https://pnml.lip6.fr/).

The grammar is constructed automatically from the [ecore model](http://www.pnml.org/grammar.php) provided by [Pnml.org](http://www.pnml.org/).
