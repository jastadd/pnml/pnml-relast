aspect Navigation {
  inh PetriNet PnObject.petriNet();
  eq PetriNet.getChild().petriNet() = this;

  syn java.util.Collection<Place> TransitionNode.incomingPlaces() {

      java.util.Set<Place> s = new java.util.HashSet<>();

      for (Arc incomingArc : getInArcList()) {
          s.add(incomingArc.getSource().asPlaceNode().place());
      }

      for (TransitionNode ref : getReferencingTransitions()) {
          s.addAll(ref.incomingPlaces());
      }

      return s;
  }
  
  syn java.util.Collection<Place> TransitionNode.outgoingPlaces() {

    java.util.Set<Place> s = new java.util.HashSet<>();

    for (Arc outgoing : getOutArcList()) {
         s.add(outgoing.getTarget().asPlaceNode().place());
    }

    for (TransitionNode ref : getReferencingTransitions()) {
         s.addAll(ref.outgoingPlaces());
    }

    return s;
  }

  inh Page PnObject.ContainingPage();
  eq Page.getObject().ContainingPage() = this;
  eq PetriNet.getPage().ContainingPage() = null;

  syn boolean Node.isPlaceNode() = false;
  eq PlaceNode.isPlaceNode() = true;

  syn PlaceNode Node.asPlaceNode() = null;
  eq PlaceNode.asPlaceNode() = this;

  syn boolean Node.isTransitionNode() = false;
  eq TransitionNode.isTransitionNode() = true;

  syn TransitionNode Node.asTransitionNode() = null;
  eq TransitionNode.asTransitionNode() = this;

  syn Place PlaceNode.place();
  eq Place.place() = this;
  eq RefPlace.place() = getRef().place();

  syn Transition TransitionNode.transition();
  eq Transition.transition() = this;
  eq RefTransition.transition() = getRef().transition();

  syn boolean PlaceNode.isRefPlace() = false;
  eq RefPlace.isRefPlace() = true;

  syn boolean TransitionNode.isTransition() = false;
  eq Transition.isTransition() = true;

  syn boolean TransitionNode.isRefTransition() = false;
  eq RefTransition.isRefTransition() = true;

  syn RefTransition TransitionNode.asRefTransition() = null;
  eq RefTransition.asRefTransition() = this;

  syn Transition TransitionNode.asTransition() = null;
  eq Transition.asTransition() = this;

  coll java.util.Set<PnObject> PetriNet.allObjects() [new java.util.HashSet()] root PetriNet;
  PnObject contributes this
    to PetriNet.allObjects()
    for petriNet();

  coll java.util.Set<Place> PetriNet.allPlaces() [new java.util.HashSet()] root PetriNet;
  Place contributes this
    to PetriNet.allPlaces()
    for petriNet();

  coll java.util.Set<Transition> PetriNet.allTransitions() [new java.util.HashSet()] root PetriNet;
  Transition contributes this
    to PetriNet.allTransitions()
    for petriNet();

  coll java.util.Set<Arc> PetriNet.allArcs() [new java.util.HashSet()] root PetriNet;
  Arc contributes this
    to PetriNet.allArcs()
    for petriNet();

  coll java.util.Set<Page> PetriNet.allPages() [new java.util.HashSet()] root PetriNet;
    Page contributes this
    to PetriNet.allPages()
    for petriNet();
}
