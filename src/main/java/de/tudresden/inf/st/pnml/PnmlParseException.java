package de.tudresden.inf.st.pnml;

public class PnmlParseException extends Exception {

  public PnmlParseException(String message) {
    super(message);
  }

  public PnmlParseException(String message, Throwable cause) {
    super(message, cause);
  }

  public PnmlParseException(Throwable cause) {
    super(cause);
  }

}
