package de.tudresden.inf.st.pnml;

import de.tudresden.inf.st.pnml.jastadd.model.*;

public class PnmlParser {

  private final PetriNet petriNet;

  public PnmlParser(fr.lip6.move.pnml.ptnet.PetriNet source) throws PnmlParseException {
    this.petriNet = getPetriNet(source);
    petriNet.treeResolveAll();
  }


  public PetriNet getPetriNet() {
    return petriNet;
  }

  // the get methods

  private PetriNet getPetriNet(fr.lip6.move.pnml.ptnet.PetriNet source) throws PnmlParseException {
    PetriNet result = new PetriNet();
    fillPetriNet(source, result);
    return result;
  }

  private Page getPage(fr.lip6.move.pnml.ptnet.Page source) throws PnmlParseException {
    Page result = new Page();
    fillPage(source, result);
    return result;
  }

  private NodeGraphics getNodeGraphics(fr.lip6.move.pnml.ptnet.NodeGraphics source) {
    NodeGraphics result = new NodeGraphics();
    fillNodeGraphics(source, result);
    return result;
  }

  private Line getLine(fr.lip6.move.pnml.ptnet.Line source) {
    Line result = new Line();
    fillLine(source, result);
    return result;
  }

  private Fill getFill(fr.lip6.move.pnml.ptnet.Fill source) {
    Fill result = new Fill();
    fillFill(source, result);
    return result;
  }

  private Dimension getDimension(fr.lip6.move.pnml.ptnet.Dimension source) {
    Dimension result = new Dimension();
    fillDimension(source, result);
    return result;
  }

  private Offset getOffset(fr.lip6.move.pnml.ptnet.Offset source) {
    Offset result = new Offset();
    fillOffset(source, result);
    return result;
  }

  private Position getPosition(fr.lip6.move.pnml.ptnet.Position source) {
    Position result = new Position();
    fillPosition(source, result);
    return result;
  }

  private Name getName(fr.lip6.move.pnml.ptnet.Name source) {
    Name result = new Name();
    fillName(source, result);
    return result;
  }

  private AnnotationGraphics getAnnotationGraphics(fr.lip6.move.pnml.ptnet.AnnotationGraphics source) {
    AnnotationGraphics result = new AnnotationGraphics();
    fillAnnotationGraphics(source, result);
    return result;
  }

  private Font getFont(fr.lip6.move.pnml.ptnet.Font source) {
    Font result = new Font();
    fillFont(source, result);
    return result;
  }

  private ToolInfo getToolInfo(fr.lip6.move.pnml.ptnet.ToolInfo source) {
    ToolInfo result = new ToolInfo();
    fillToolInfo(source, result);
    return result;
  }

  private Node getNode(fr.lip6.move.pnml.ptnet.Node source) throws PnmlParseException {
    if (source instanceof fr.lip6.move.pnml.ptnet.PlaceNode) {
      return getPlaceNode((fr.lip6.move.pnml.ptnet.PlaceNode) source);
    } else if (source instanceof fr.lip6.move.pnml.ptnet.TransitionNode) {
      return getTransitionNode((fr.lip6.move.pnml.ptnet.TransitionNode) source);
    } else {
      throw new PnmlParseException("Encountered illegal unknown subtype of Node " + source.getClass().getCanonicalName());
    }
  }

  private PlaceNode getPlaceNode(fr.lip6.move.pnml.ptnet.PlaceNode source) throws PnmlParseException {
    if (source instanceof fr.lip6.move.pnml.ptnet.Place) {
      return getPlace((fr.lip6.move.pnml.ptnet.Place) source);
    } else if (source instanceof fr.lip6.move.pnml.ptnet.RefPlace) {
      return getRefPlace((fr.lip6.move.pnml.ptnet.RefPlace) source);
    } else {
      throw new PnmlParseException("Encountered illegal unknown subtype of PlaceNode " + source.getClass().getCanonicalName());
    }
  }

  private Place getPlace(fr.lip6.move.pnml.ptnet.Place source) {
    Place result = new Place();
    fillPlace(source, result);
    return result;
  }

  private PTMarking getPTMarking(fr.lip6.move.pnml.ptnet.PTMarking source) {
    PTMarking result = new PTMarking();
    fillPTMarking(source, result);
    return result;
  }

  private RefPlace getRefPlace(fr.lip6.move.pnml.ptnet.RefPlace source) {
    RefPlace result = new RefPlace();
    fillRefPlace(source, result);
    return result;
  }

  private TransitionNode getTransitionNode(fr.lip6.move.pnml.ptnet.TransitionNode source) throws PnmlParseException {
    if (source instanceof fr.lip6.move.pnml.ptnet.Transition) {
      return getTransition((fr.lip6.move.pnml.ptnet.Transition) source);
    } else if (source instanceof fr.lip6.move.pnml.ptnet.RefTransition) {
      return getRefTransition((fr.lip6.move.pnml.ptnet.RefTransition) source);
    } else {
      throw new PnmlParseException("Encountered illegal unknown subtype of TransitionNode " + source.getClass().getCanonicalName());
    }
  }

  private RefTransition getRefTransition(fr.lip6.move.pnml.ptnet.RefTransition source) {
    RefTransition result = new RefTransition();
    fillRefTransition(source, result);
    return result;
  }

  private Transition getTransition(fr.lip6.move.pnml.ptnet.Transition source) {
    Transition result = new Transition();
    fillTransition(source, result);
    return result;
  }

  private Arc getArc(fr.lip6.move.pnml.ptnet.Arc source) {
    Arc result = new Arc();
    fillArc(source, result);
    return result;
  }

  private PTArcAnnotation getPTArcannotation(fr.lip6.move.pnml.ptnet.PTArcAnnotation source) {
    PTArcAnnotation result = new PTArcAnnotation();
    fillPTAnnotation(source, result);
    return result;
  }

  private ArcGraphics getArcGraphics(fr.lip6.move.pnml.ptnet.ArcGraphics source) {
    ArcGraphics result = new ArcGraphics();
    fillArcGraphics(source, result);
    return result;
  }

  private PnObject getPnObject(fr.lip6.move.pnml.ptnet.PnObject source) throws PnmlParseException {
    if (source instanceof fr.lip6.move.pnml.ptnet.Page) {
      return getPage((fr.lip6.move.pnml.ptnet.Page) source);
    } else if (source instanceof Node) {
      return getNode((fr.lip6.move.pnml.ptnet.Node) source);
    } else if (source instanceof fr.lip6.move.pnml.ptnet.Arc) {
      return getArc((fr.lip6.move.pnml.ptnet.Arc) source);
    } else if (source instanceof fr.lip6.move.pnml.ptnet.Node) {
      return getNode((fr.lip6.move.pnml.ptnet.Node) source);
    } else {
      throw new PnmlParseException("Encountered illegal unknown subtype of PnObject " + source.getClass().getCanonicalName());
    }
  }

  // the fill methods

  private void fillPetriNet(fr.lip6.move.pnml.ptnet.PetriNet source, PetriNet result) throws PnmlParseException {
    for (fr.lip6.move.pnml.ptnet.Page page : source.getPages()) {
      result.addPage(getPage(page));
    }
    if (source.getName() != null) {
      result.setName(getName(source.getName()));
    }
    for (fr.lip6.move.pnml.ptnet.ToolInfo toolInfo : source.getToolspecifics()) {
      result.addToolspecific(getToolInfo(toolInfo));
    }
    result.setId(source.getId());
    if (source.getType() == fr.lip6.move.pnml.ptnet.PNType.PTNET) {
      result.setType(PNType.PTNET);
    } else {
      throw new PnmlParseException("Illegal Petri Net type '" + source.getType().getLiteral() + "' encountered.");
    }
  }

  private void fillPage(fr.lip6.move.pnml.ptnet.Page source, Page result) throws PnmlParseException {
    fillPnObject(source, result);

    for (fr.lip6.move.pnml.ptnet.PnObject object : source.getObjects()) {
      result.addObject(getPnObject(object));
    }
    if (source.getNodegraphics() != null) {
      result.setNodeGraphics(getNodeGraphics(source.getNodegraphics()));
    }
  }

  private void fillPnObject(fr.lip6.move.pnml.ptnet.PnObject source, PnObject result) {
    if (source.getName() != null) {
      result.setName(getName(source.getName()));
    }
    for (fr.lip6.move.pnml.ptnet.ToolInfo toolInfo : source.getToolspecifics()) {
      result.addToolspecific(getToolInfo(toolInfo));
    }
    result.setId(source.getId());
  }

  private void fillNodeGraphics(fr.lip6.move.pnml.ptnet.NodeGraphics source, NodeGraphics result) {
    if (source.getPosition() != null) {
      result.setPosition(getPosition(source.getPosition()));
    }
    if (source.getDimension() != null) {
      result.setDimension(getDimension(source.getDimension()));
    }
    if (source.getFill() != null) {
      result.setFill(getFill(source.getFill()));
    }
    if (source.getLine() != null) {
      result.setLine(getLine(source.getLine()));
    }
  }

  private void fillLine(fr.lip6.move.pnml.ptnet.Line source, Line result) {
    result.setColor(CSS2Color.valueOf(source.getColor().getName()));
    result.setShape(LineShape.valueOf(source.getShape().getName()));
    result.setWidth(source.getWidth());
    result.setStyle(LineStyle.valueOf(source.getStyle().getName()));
  }

  private void fillFill(fr.lip6.move.pnml.ptnet.Fill source, Fill result) {
    result.setColor(CSS2Color.valueOf(source.getColor().getName()));
    result.setGradientcolor(CSS2Color.valueOf(source.getGradientcolor().getName()));
    result.setGradientrotation(Gradient.valueOf(source.getGradientrotation().getName()));
    result.setImage(source.getImage());
  }

  private void fillDimension(fr.lip6.move.pnml.ptnet.Dimension source, Dimension result) {
    fillCoordinate(source, result);
  }

  private void fillCoordinate(fr.lip6.move.pnml.ptnet.Coordinate source, Coordinate result) {
    result.setX(source.getX());
    result.setY(source.getY());
  }

  private void fillOffset(fr.lip6.move.pnml.ptnet.Offset source, Offset result) {
    fillCoordinate(source, result);
  }

  private void fillPosition(fr.lip6.move.pnml.ptnet.Position source, Position result) {
    fillCoordinate(source, result);
  }

  private void fillName(fr.lip6.move.pnml.ptnet.Name source, Name result) {
    fillAnnotation(source, result);

    result.setText(source.getText());
  }

  private void fillAnnotation(fr.lip6.move.pnml.ptnet.Annotation source, Annotation result) {
    if (source.getAnnotationgraphics() != null) {
      result.setAnnotationGraphics(getAnnotationGraphics(source.getAnnotationgraphics()));
    }
    fillLabel(source, result);
  }

  private void fillLabel(fr.lip6.move.pnml.ptnet.Annotation source, Annotation result) {
    for (fr.lip6.move.pnml.ptnet.ToolInfo info : source.getToolspecifics()) {
      result.addToolspecific(getToolInfo(info));
    }
  }

  private void fillAnnotationGraphics(fr.lip6.move.pnml.ptnet.AnnotationGraphics source, AnnotationGraphics result) {
    if (source.getOffset() != null) {
      result.setOffset(getOffset(source.getOffset()));
    }
    if (source.getFill() != null) {
      result.setFill(getFill(source.getFill()));
    }
    if (source.getLine() != null) {
      result.setLine(getLine(source.getLine()));
    }
    if (source.getFont() != null) {
      result.setFont(getFont(source.getFont()));
    }
  }

  private void fillFont(fr.lip6.move.pnml.ptnet.Font source, Font result) {
    result.setAlign(FontAlign.valueOf(source.getAlign().getName()));
    result.setDecoration(FontDecoration.valueOf(source.getDecoration().getName()));
    result.setFamily(CSS2FontFamily.valueOf(source.getFamily().getName()));
    result.setRotation(source.getRotation());
    result.setSize(CSS2FontSize.valueOf(source.getSize().getName()));
    result.setStyle(CSS2FontStyle.valueOf(source.getStyle().getName()));
    result.setWeight(CSS2FontWeight.valueOf(source.getWeight().getName()));
  }

  private void fillToolInfo(fr.lip6.move.pnml.ptnet.ToolInfo source, ToolInfo result) {
    result.setTool(source.getTool());
    result.setVersion(source.getVersion());
    result.setFormattedXMLBuffer(source.getFormattedXMLBuffer());
    result.setToolInfoGrammarURI(source.getToolInfoGrammarURI());
  }

  private void fillPlace(fr.lip6.move.pnml.ptnet.Place source, Place result) {
    fillPlaceNode(source, result);

    if (source.getInitialMarking() != null) {
      result.setInitialMarking(getPTMarking(source.getInitialMarking()));
    }
  }

  private void fillPTMarking(fr.lip6.move.pnml.ptnet.PTMarking source, PTMarking result) {
    fillAnnotation(source, result);

    // whatever this is supposed to be. they changed the Integer to Long
    result.setText(Math.toIntExact(source.getText()));
  }

  private void fillRefPlace(fr.lip6.move.pnml.ptnet.RefPlace source, RefPlace result) {
    fillPlaceNode(source, result);

    result.setRef(PlaceNode.createRefDirection(source.getRef().getId()));
  }

  private void fillPlaceNode(fr.lip6.move.pnml.ptnet.PlaceNode source, PlaceNode result) {
    fillNode(source, result);

    for (fr.lip6.move.pnml.ptnet.RefPlace referencingPlace : source.getReferencingPlaces()) {
      result.addReferencingPlace(RefPlace.createRefDirection(referencingPlace.getId()));
    }
  }

  private void fillRefTransition(fr.lip6.move.pnml.ptnet.RefTransition source, RefTransition result) {
    fillTransitionNode(source, result);

    result.setRef(TransitionNode.createRefDirection(source.getRef().getId()));
  }

  private void fillTransitionNode(fr.lip6.move.pnml.ptnet.TransitionNode source, TransitionNode result) {
    fillNode(source, result);

    for (fr.lip6.move.pnml.ptnet.RefTransition refTransition : source.getReferencingTransitions()) {
      result.addReferencingTransition(RefTransition.createRefDirection(refTransition.getId()));
    }
  }

  private void fillTransition(fr.lip6.move.pnml.ptnet.Transition source, Transition result) {
    fillTransitionNode(source, result);
  }

  private void fillNode(fr.lip6.move.pnml.ptnet.Node source, Node result) {

    fillPnObject(source, result);

    if (source.getNodegraphics() != null) {
      result.setNodeGraphics(getNodeGraphics(source.getNodegraphics()));
    }

    for (fr.lip6.move.pnml.ptnet.Arc outArc : source.getOutArcs()) {
      result.addOutArc(Arc.createRefDirection(outArc.getId()));
    }
    for (fr.lip6.move.pnml.ptnet.Arc inArc : source.getInArcs()) {
      result.addInArc(Arc.createRefDirection(inArc.getId()));
    }
  }

  private void fillArc(fr.lip6.move.pnml.ptnet.Arc source, Arc result) {
    fillPnObject(source, result);

    if (source.getArcgraphics() != null) {
      result.setArcGraphics(getArcGraphics(source.getArcgraphics()));
    }
    if (source.getInscription() != null) {
      result.setInscription(getPTArcannotation(source.getInscription()));
    }

    result.setSource(Node.createRefDirection(source.getSource().getId()));
    result.setTarget(Node.createRefDirection(source.getTarget().getId()));
  }

  private void fillPTAnnotation(fr.lip6.move.pnml.ptnet.PTArcAnnotation source, PTArcAnnotation result) {
    fillAnnotation(source, result);

    // whatever this is supposed to be. they changed the Integer to Long
    result.setText(Math.toIntExact(source.getText()));
  }

  private void fillArcGraphics(fr.lip6.move.pnml.ptnet.ArcGraphics source, ArcGraphics result) {
    for (fr.lip6.move.pnml.ptnet.Position position : source.getPositions()) {
      result.addPosition(getPosition(position));
    }
    if (source.getLine() != null) {
      result.setLine(getLine(source.getLine()));
    }
  }

}
