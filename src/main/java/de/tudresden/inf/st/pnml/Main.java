package de.tudresden.inf.st.pnml;

import de.tudresden.inf.st.pnml.jastadd.model.Marking;
import de.tudresden.inf.st.pnml.jastadd.model.PetriNet;

import fr.lip6.move.pnml.framework.general.PNType;
import fr.lip6.move.pnml.framework.hlapi.HLAPIRootClass;
import fr.lip6.move.pnml.framework.utils.ModelRepository;
import fr.lip6.move.pnml.framework.utils.PNMLUtils;
import fr.lip6.move.pnml.framework.utils.exception.ImportException;
import fr.lip6.move.pnml.framework.utils.exception.InvalidIDException;
import fr.lip6.move.pnml.ptnet.hlapi.PetriNetDocHLAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class Main {

  private static final Logger logger = LoggerFactory.getLogger(Main.class);

  public static void main(String[] args) {

//    String fileName = "src/main/resources/minimal.pnml";
//    String fileName = "src/main/resources/philo.pnml";
    String fileName = "src/main/resources/haddadin_automaton_flat.pnml";

    List<PetriNet> petriNets = parsePnml(fileName);

    for (PetriNet petriNet : petriNets) {
      Marking originalMarking = petriNet.initialMarking();
      Marking currentMarking = originalMarking;
      logger.info("initial marking:\n{}", originalMarking.print());

      // always use seed 0, it is the best seed!
      Random random1 = new Random(0);
      Random random2 = new Random(0);

      for (int i = 1; i <= 5 && !originalMarking.isDead() && !currentMarking.isDead(); i++) {

        Optional<Marking> optionalCurrentMarking = currentMarking.fire(random1);
        if (optionalCurrentMarking.isPresent()) {
          currentMarking = optionalCurrentMarking.get();
        } else {
          throw new RuntimeException("Unable to get successor marking, even though the marking is not dead!");
        }
        originalMarking.fireInPlace(random2);
        logger.info("Original marking after {} firings: {}", i, originalMarking.print());
        logger.info("Current marking after {} firings: {}", i, currentMarking.print());
      }

      logger.info("DOT file:\n\n{}", petriNet.toDot());
    }
  }

  private static List<PetriNet> parsePnml(String fileName) {
    Path file = Paths.get(fileName);

    HLAPIRootClass document = null;

    try {
      document = PNMLUtils.importPnmlDocument(file.toFile(), false);

      logger.info(document.toPNML());
    } catch (ImportException | InvalidIDException e) {
      logger.error("Unable to import PNML document from file '{}'", fileName);
      logger.error("Exception was thrown!", e);
      System.exit(-1);
    }


    logger.info("Imported document workspace ID: {}", ModelRepository.getInstance().getCurrentDocWSId());

    List<PetriNet> petriNets = new ArrayList<>();

    PNType type = PNMLUtils.determinePNType(document);
    switch (type) {
      case PTNET:
        PetriNetDocHLAPI ptDoc =
            (PetriNetDocHLAPI) document;



        for (fr.lip6.move.pnml.ptnet.PetriNet pmnlNet : ptDoc.getNets()) {
          PnmlParser parser;
          try {
            parser = new PnmlParser(pmnlNet);
            petriNets.add(parser.getPetriNet());
          } catch (PnmlParseException e) {
            logger.error("Parsing the Petri net using the PNML framework failed.", e);
          }
        }

        break;
      case COREMODEL:
      case SYMNET:
      case HLPN:
      case PTHLPN:
      default:
        logger.error("Petri net is of unsupported type {}.", type.getLiteral());
        System.exit(-1);
    }
    return petriNets;
  }

}
